package userinput;

import java.util.Scanner;

public class MainMethodForConverting {

    public static final String path = "/home/malhar/NetBeansProjects/xml-csv/src/main/java/com/project/readFile/AkshayFile.xml";

    public static void main(String[] args) {

        String commonPath = "/";
        Scanner sc =  new Scanner(System.in);
        System.out.println("Enter the filename with file path!!");
        String filePath = sc.nextLine();
        if(!filePath.startsWith("/"))
            filePath = "/"+filePath;
        String[] arr = filePath.split("/");
        String commonFileName = arr[arr.length-1];
        commonFileName = commonFileName.substring(0, commonFileName.indexOf('.'));
        for(int i=1; i < arr.length-1 ; i++ ) {
            commonPath = commonPath+arr[i]+"/";
        }
        String xlsFileLocation =  commonPath+commonFileName+".xsl";
        String outputFileName = commonFileName+".csv";

        XmlToCsvFileConverter xmlToCsvFileCoverter =  new XmlToCsvFileConverter();
        xmlToCsvFileCoverter.convertXmlToCsvFile(filePath, xlsFileLocation, commonPath, outputFileName);

    }
    
}
